package com.gradgoose.quickmath;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Date;

public class MathActivity extends Activity {
	
	final long s = 1000; // 1 second. 
	
	long allowedAnswerTime = 2000; // 2 seconds. 
	double actualAnswerFrequency = 0.4; // Show actual answer 40% of the time. 
	
	TextView tvMathExp; 
	ProgressBar pbTimeLeft; 
	
	View btnThinkCorrect; 
	View btnThinkIncorrect; 
	
	View lPauseOverlay; 
	TextView tvOverlayTitle; 
	TextView tvScoreSummary; 
	Button btnResume; 
	
	TextView tvClearScores; 
	TextView tvAnswerTimeCover; 
	
	NumberPicker pTimeAdd; 
	NumberPicker pTimeSub; 
	NumberPicker pTimeMul; 
	NumberPicker pTimeDiv; 
	
	SharedPreferences mOptions; 
	SharedPreferences mScore; 
	
	Handler mHandler = new Handler (); 
	
	boolean mTimerRunning = false; 
	Runnable mBgTimerTask = new Runnable () { 
		@Override public void run () { 
			runOnUiThread (mUiTimerTick); 
			if (!isPaused && !activityPaused) { 
				mHandler.postDelayed (this, 50); 
				mTimerRunning = true; 
			} else mTimerRunning = false; 
		} 
	}; 
	long answerTimeStart = 0; 
	Runnable mUiTimerTick = new Runnable () { 
		@Override public void run () { 
			long now = System.currentTimeMillis (); 
			long elapsed = now - answerTimeStart; 
			long remaining = allowedAnswerTime - elapsed; 
			int percent = (int) (remaining * 100 / allowedAnswerTime); 
			if (percent < 0) { 
				percent = 0; 
				gameOver (); 
			} 
			pbTimeLeft.setProgress (percent); 
		} 
	}; 
	
	@Override protected void onCreate (Bundle savedInstanceState) { 
		super.onCreate (savedInstanceState); 
		setContentView (R.layout.activity_math); 
		tvMathExp = findViewById (R.id.tvMathExp); 
		pbTimeLeft = findViewById (R.id.timeLeft); 
		btnThinkCorrect = findViewById (R.id.btnThinkCorrect); 
		btnThinkCorrect.setOnClickListener (myCheckAnswer); 
		btnThinkIncorrect = findViewById (R.id.btnThinkIncorrect); 
		btnThinkIncorrect.setOnClickListener (myCheckAnswer); 
		lPauseOverlay = findViewById (R.id.lPauseOverlay); 
		tvOverlayTitle = findViewById (R.id.tvOverlayTitle); 
		tvScoreSummary = findViewById (R.id.tvScoreSummary); 
		btnResume = findViewById (R.id.btnResume); 
		tvClearScores = findViewById (R.id.tvClearScores); 
		tvClearScores.setOnClickListener (new View.OnClickListener () { 
			@Override public void onClick (View view) { 
				clearScores (); 
				refreshScoreSummary (); 
			} 
		}); 
		tvAnswerTimeCover = findViewById (R.id.tvAnswerTimeCover); 
		tvAnswerTimeCover.setOnClickListener (new View.OnClickListener () { 
			@Override public void onClick (View view) { 
				view.setVisibility (View.GONE); 
			} 
		}); 
		pTimeAdd = findViewById (R.id.pTimeAdd); 
		pTimeSub = findViewById (R.id.pTimeSub); 
		pTimeMul = findViewById (R.id.pTimeMul); 
		pTimeDiv = findViewById (R.id.pTimeDiv); 
		// Get data: 
		mOptions = getSharedPreferences ("options", MODE_PRIVATE); 
		mScore = getSharedPreferences ("score", MODE_PRIVATE); 
		// Hide unnecessary: 
		lPauseOverlay.setVisibility (View.GONE); 
		// Fill things out: 
		initNumberPickers (); 
		// Start: 
		updateGameScore (0); 
		loadNext (); 
	} 
	
	@Override public void onSaveInstanceState (Bundle outState) { 
		outState.putBoolean ("isPaused", isPaused); 
	} 
	@Override public void onRestoreInstanceState (Bundle inState) { 
		pauseGame (inState.getBoolean ("isPaused")); 
	} 
	
	@Override public boolean onCreateOptionsMenu (Menu menu) { 
		getMenuInflater ().inflate (R.menu.menu_math, menu); 
		return true; 
	} 
	
	boolean isPaused = false; 
	MenuItem pauseItem; 
	@Override public boolean onPrepareOptionsMenu (Menu menu) { 
		pauseItem = menu.findItem (R.id.menu_action_pause); 
		if (isPaused) pauseGame (true); // Update the menu icon. 
		return true; 
	} 
	
	@Override public boolean onOptionsItemSelected (MenuItem item) { 
		switch (item.getItemId ()) { 
			case R.id.menu_action_pause: 
				if (rOnResume != null) { 
					lOnResumeButton.onClick (btnResume); 
				} else pauseGame (!isPaused); 
				break; 
			default: 
				return super.onOptionsItemSelected (item); 
		} 
		return true; 
	} 
	
	boolean activityPaused = true; 
	@Override protected void onPause () { 
		activityPaused = true; 
		super.onPause (); 
	} 
	@Override protected void onResume () { 
		super.onResume (); 
		activityPaused = false; 
		if (!mTimerRunning) 
			mBgTimerTask.run (); 
	} 
	
	private void initNumberPickers () { 
		NumberPicker.OnValueChangeListener numberPicked = new NumberPicker.OnValueChangeListener () { 
			@Override public void onValueChange (NumberPicker numberPicker, int oldVal, int nowVal) { 
				char op = 0; 
				if (numberPicker == pTimeAdd) 
					op = '+'; 
				else if (numberPicker == pTimeSub) 
					op = '-'; 
				else if (numberPicker == pTimeMul) 
					op = '*'; 
				else if (numberPicker == pTimeDiv) 
					op = '/'; 
				if (op != 0) setAllowedAnswerTime (op, nowVal * s); 
			} 
		}; 
		pTimeAdd.setMinValue (1); 
		pTimeAdd.setMaxValue (9); 
		pTimeAdd.setOnValueChangedListener (numberPicked); 
		pTimeSub.setMinValue (1); 
		pTimeSub.setMaxValue (9); 
		pTimeSub.setOnValueChangedListener (numberPicked); 
		pTimeMul.setMinValue (1); 
		pTimeMul.setMaxValue (9); 
		pTimeMul.setOnValueChangedListener (numberPicked); 
		pTimeDiv.setMinValue (1); 
		pTimeDiv.setMaxValue (9); 
		pTimeDiv.setOnValueChangedListener (numberPicked); 
		updateNumberPickers (); 
	} 
	void updateNumberPickers () { 
		pTimeAdd.setValue ((int) (getAllowedAnswerTime ('+') / 1000)); 
		pTimeSub.setValue ((int) (getAllowedAnswerTime ('-') / 1000)); 
		pTimeMul.setValue ((int) (getAllowedAnswerTime ('*') / 1000)); 
		pTimeDiv.setValue ((int) (getAllowedAnswerTime ('/') / 1000)); 
	} 
	
	View.OnClickListener myCheckAnswer = new View.OnClickListener () { 
		@Override public void onClick (View view) { 
			boolean thinkCorrect = view == btnThinkCorrect; 
			boolean answerCorrect = answerShown == question.answer; 
			if (thinkCorrect ^ answerCorrect) { 
				gameOver (); 
			} else { 
				updateGameScore (1); 
				loadNext (); 
			} 
		} 
	}; 
	
	Runnable myResume = new Runnable () { 
		@Override public void run () { 
			pauseGame (false); 
			loadNext (); 
		} 
	}; 
	Runnable myNewGame = new Runnable () { 
		@Override public void run () { 
			updateGameScore (0); 
			loadNext (); 
		} 
	}; 
	
	void pauseGame (boolean needPause) { 
		isPaused = needPause; 
		if (pauseItem != null) 
			pauseItem.setIcon (needPause ? R.drawable.ic_play_arrow_white_24dp : 
			R.drawable.ic_pause_white_24dp); 
		if (needPause) { 
			showPauseOverlay (R.string.title_paused, R.string.resume, myResume); 
		} 
	} 
	
	void gameOver () { 
		startGameScore (); // Save current time and all ... 
		showPauseOverlay (R.string.title_over, R.string.more, myNewGame); 
	} 
	
	Runnable rOnResume = null; 
	Runnable rHandleResumeButton = new Runnable () { 
		@Override public void run () { 
			if (pauseItem != null) 
				pauseItem.setIcon (R.drawable.ic_pause_white_24dp); 
			lPauseOverlay.setVisibility (View.GONE); 
			if (rOnResume != null) 
				rOnResume.run (); 
			rOnResume = null; // Reset to NULL. 
			isPaused = false; 
			if (!mTimerRunning) 
				mBgTimerTask.run (); 
		} 
	}; 
	View.OnClickListener lOnResumeButton = new View.OnClickListener () { 
		@Override public void onClick (View view) { 
			runOnUiThread (rHandleResumeButton); 
		} 
	}; 
	void showPauseOverlay (int titleText, int resumeButtonText, Runnable onResume) { 
		rOnResume = onResume; 
		isPaused = true; 
		if (pauseItem != null) 
			pauseItem.setIcon (R.drawable.ic_play_arrow_white_24dp); 
		lPauseOverlay.setVisibility (View.VISIBLE); 
		tvOverlayTitle.setText (titleText); 
		btnResume.setText (resumeButtonText); 
		btnResume.setOnClickListener (lOnResumeButton); 
		tvAnswerTimeCover.setVisibility (View.VISIBLE); 
		refreshScoreSummary (); 
		updateNumberPickers (); 
	} 
	
	void refreshScoreSummary () { 
		long now = System.currentTimeMillis (); 
		int currentIndex = mScore.getInt ("current-game", 0); 
		if (currentIndex > 0 && mScore.getInt ("scores[" + currentIndex + "]", -1) == -1) 
			currentIndex--; 
		int currentScore = mScore.getInt ("scores[" + currentIndex + "]", 0); 
		long currentStart = mScore.getLong ("game-start[" + currentIndex + "]", 0); 
		long currentEnd = mScore.getLong ("game-end[" + currentIndex + "]", now); 
		long currentTimeElapsed = currentEnd - currentStart; 
		String currentTime = currentStart != 0 && currentScore != 0 ? 
								  String.valueOf ((float) currentTimeElapsed / currentScore / 1000) 
									 : "N/A"; 
		int bestScoreIndex = 0; 
		int bestScore = mScore.getInt ("scores[0]", 0); 
		int bestTimeIndex = -1; 
		int bestTime = 0; 
		int avgScore = 0; 
		int avgTime = 0; 
		int count = 0; 
		for (int i = 0; i <= currentIndex; i++, count++) { 
			int score = mScore.getInt ("scores[" + i + "]", -1); 
			if (score == -1) break; 
			avgScore += score; 
			if (score > bestScore) { 
				bestScoreIndex = i; 
				bestScore = score; 
			} 
			if (score == 0) 
				continue; 
			long start = mScore.getLong ("game-start[" + i + "]", 0); 
			if (start == 0) continue; 
			long stop = mScore.getLong ("game-end[" + i + "]", now); 
			int time = (int) ((stop - start) / score); 
			avgTime += time; 
			if (bestTimeIndex == -1 || time < bestTime) { 
				bestTimeIndex = i; 
				bestTime = time; 
			} 
		} 
		if (count > 0) { 
			avgScore /= count; 
			avgTime /= count; 
		} 
		long bestTimeWas = mScore.getLong ("game-end[" + bestTimeIndex + "]", now); 
		long bestScoreWas = mScore.getLong ("game-end[" + bestScoreIndex + "]", now);
		Date dateBestTime = new Date (bestTimeWas); 
		Date dateBestScore = new Date (bestScoreWas); 
		java.text.DateFormat dateFormat = DateFormat.getDateFormat (this); 
		java.text.DateFormat timeFormat = DateFormat.getTimeFormat (this); 
		String whenBestTime = dateFormat.format (dateBestTime) + " " + timeFormat.format (dateBestTime); 
		String whenBestScore = dateFormat.format (dateBestScore) + " " + timeFormat.format (dateBestScore); 
		if (bestTimeIndex < 0 || count == 0) 
			whenBestScore = whenBestTime = "N/A"; 
		// Now fill out a text template with these numbers: 
		String template = getString (R.string.score_summary); 
		String result = template 
				.replace ("{current-score}", String.valueOf (currentScore)) 
				.replace ("{current-time}", currentTime) 
				.replace ("{avg-time}", String.valueOf ((float) avgTime / 1e3f)) 
				.replace ("{avg-score}", String.valueOf (avgScore)) 
				.replace ("{best-time}", String.valueOf ((float) bestTime / 1e3f)) 
				.replace ("{best-score}", String.valueOf (bestScore)) 
				.replace ("{best-time-date}", whenBestTime) 
				.replace ("{best-score-date}", whenBestScore) 
				.replace ("{endl}", "\n"); 
		tvScoreSummary.setText (result); 
	} 
	
	static class Question { 
		static char availableOperators [] = { '+', '-', '*', '/' }; 
		int a = 0; 
		int b = 0; 
		char op = '+'; 
		int answer = 0; 
		void askRandom () { askRandom ((char) 0); } 
		void askRandom (char needOp) { 
			if (needOp != 0) 
				op = needOp; 
			else op = availableOperators[(int) Math.floor (4 * Math.random ())]; 
			b = (int) Math.floor (8 * Math.random ()) + 1; 
			int c = (int) Math.floor (8 * Math.random ()) + 1; 
			if (op == '/') { 
				// To make the numbers end up round, we handle division questions separately: 
				answer = c; 
				a = b * c; 
			} else { 
				// The rest are straight-forward: 
				a = c; 
				switch (op) { 
					case '*': 
						answer = a * b; 
						break; 
					case '-': 
						answer = a - b; 
						break; 
					case '+': 
					default: 
						answer = a + b; 
				} 
			} 
		} 
	} 
	Question question = new Question (); 
	Question another = new Question (); 
	int answerShown = 0; 
	void loadNext () { 
		// Make up a math problem: 
		question.askRandom (); 
		another.askRandom (question.op); 
		answerShown = Math.random () < actualAnswerFrequency ? 
							  question.answer : another.answer; 
		// Show the question on the screen: 
		if (tvMathExp != null) { 
			String q = question.a + " " + question.op + " " + question.b + " = " + answerShown; 
			tvMathExp.setText (q); 
		} 
		// Put the timer stuff on the screen: 
		allowedAnswerTime = getAllowedAnswerTime (question.op); 
		answerTimeStart = System.currentTimeMillis (); 
		if (!mTimerRunning) mBgTimerTask.run (); 
	} 
	
	void updateGameScore (int delta) { 
		int currentIndex = mScore.getInt ("current-game", 0); 
		int oldScore = mScore.getInt ("scores[" + currentIndex + "]", 0); 
		SharedPreferences.Editor editor = mScore.edit () 
				.putInt ("scores[" + currentIndex + "]", oldScore + delta); 
		if (oldScore == 0 && delta == 0) { 
			editor.putLong ("game-start[" + currentIndex + "]", System.currentTimeMillis ()); 
		} 
		editor.apply (); 
	} 
	void startGameScore () { 
		int prevIndex = mScore.getInt ("current-game", 0); 
		int currentIndex = prevIndex + 1; 
		// Start with a zero score: 
		SharedPreferences.Editor editor = mScore.edit () 
				.putInt ("current-game", currentIndex); 
		editor.putLong ("game-end[" + prevIndex + "]", System.currentTimeMillis ()); 
		editor.apply (); 
	} 
	void clearScores () { 
		mScore.edit ().clear ().apply (); 
	} 
	
	long getAllowedAnswerTime (char op) { 
		long def = 3 * s; 
		if (op == '*') 
			def = 4 * s; 
		else if (op == '/') 
			def = 5 * s; 
		return mOptions.getLong ("time:" + op, def); 
	} 
	void setAllowedAnswerTime (char op, long time) { 
		mOptions.edit ().putLong ("time:" + op, time).apply (); 
	} 
	
	
}
